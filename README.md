# Welcome to my SWAPI challenge!

This is a challenge that I did for spanish company. 

I use the SWAPI (Star Wars API) to get the info (back).

The front was developed with  Vue js.

Finally, I used the library ***axios*** to connect with the api.

## Steps to use the app.

### Clone the repository.

*Clone the repository in local folder in your system.*

`git clone git@gitlab.com:MajdiKB/innovation-strategies.git`

## Install the dependencies.

*In the folder where you cloned the repository, execute:*

`npm i`

## Run the app.

*Finally run the app.*

`npm run serve`
***
*Majdi Kokaly.*
[LinkedIn](https://www.linkedin.com/in/majdi-kokaly/)