import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    // path: "*",
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: Home,
    redirect: "/",
  },
  {
    path: "/people/page/:page",
    name: "people",
    component: () => import("../views/People.vue"),
  },
  {
    path: "/people/:id",
    name: "peopleDetails",
    component: () => import("../views/PeopleDetails.vue"),
  },
  {
    path: "/starships/page/:page",
    name: "starships",
    component: () => import("../views/Starships.vue"),
  },
  {
    path: "/starships/:id",
    name: "starshipsDetails",
    component: () => import("../views/StarshipsDetails.vue"),
  },
  {
    path: "/planets/page/:page",
    name: "planets",
    component: () => import("../views/Planets.vue"),
  },
  {
    path: "/planets/:id",
    name: "planetsDetails",
    component: () => import("../views/PlanetDetails.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
