export default interface DataResultModel {
  birth_year: string;
  created: string;
  edited: string;
  eye_color: string;
  films: 4[];
  gender: string;
  hair_color: string;
  height: string;
  homeworld: string;
  mass: string;
  name: string;
  skin_color: string;
  species: [];
  starships: [];
  url: string;
  vehicles: [];
}
