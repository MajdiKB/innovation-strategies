import DataResultModel from "./DataResultModel";
export default interface DataListModel {
  count: number;
  next: string;
  previous: string;
  results: DataResultModel[];
}
