import DataListModel from "./DataListModel";
export default interface ResponseApiModel {
  config: {};
  data: DataListModel;
  headers: {};
  request: XMLHttpRequest;
  status: number;
  statusText: string,
}
