import axios from "axios";
import ResponseApiModel from "../models/ResponseApiModel";

const API: string = process.env.VUE_APP_URL_API;

export default {
  get(id: string, section: string): Promise<ResponseApiModel> {
    return axios.get(API + section + "/" + id);
  },
};
