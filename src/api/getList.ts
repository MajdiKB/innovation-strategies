import axios from "axios";
import ResponseApiModel from "../models/ResponseApiModel";
const API: string = process.env.VUE_APP_URL_API;

export default {
  get(section: string, page: string): Promise<ResponseApiModel> {
    return axios.get(API + section + "/?page=" + page);
  },
};
